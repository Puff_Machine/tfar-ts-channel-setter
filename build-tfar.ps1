$str1 = 'force force TFAR_Teamspeak_Channel_Name = "TFR: '
$str2 = '";'
$addonBuilder = Join-Path (Get-ItemProperty "HKCU:\Software\Bohemia Interactive\Arma 3 Tools").path "AddonBuilder\AddonBuilder.exe"

For($i=1; $i -le 9; $i++)
{
    "$str1$i$str2" > ".\tfar-ts-channel-setter\cba_settings_userconfig\cba_settings.sqf"
    New-Item -Path "." -Name "@tfar-ts-channel-setter-$i" -ItemType "directory"
    & "$addonBuilder" "P:\hob\tfar-ts-channel-setter\cba_settings_userconfig" "P:\hob\@tfar-ts-channel-setter-$i\addons\cba_settings_userconfig.pbo" -prefix="cba_settings_userconfig" -clear -include="include.txt"
    Rename-Item "P:\hob\@tfar-ts-channel-setter-$i\addons\cba_settings_userconfig.pbo" "out"
    Move-Item "P:\hob\@tfar-ts-channel-setter-$i\addons\out\cba_settings_userconfig.pbo" "P:\hob\@tfar-ts-channel-setter-$i\addons\"
    Remove-Item "P:\hob\@tfar-ts-channel-setter-$i\addons\out"
}