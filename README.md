# TFAR TS Channel Setter
This mod aims to deal with an outstanding bug involving tfar beta and multiple arma servers using the same tfar ts channel by automatically generating duplicates of the mod with an incrimenting tfar ts server channel name. 

## FASTER Installation Instructions
1. Grab the zip file from [Here](https://gitlab.com/501st-legion-starsim/tfar-ts-channel-setter/-/blob/master/TFAR-TS-Channel-Setter-V3.zip)
2. Extract that zip file into the root of the arma 3 server folder, next to arma3server.exe
3. For each Server Profile, Select that profile, goto the Profile tab, Select Load From File to load the servers modlist html file 

![image.png](./docs/image.png)

4. Right click the server coloumn header, and select copy from client to set all the client mods

![image-1.png](./docs/image-1.png)

5. scroll down and enable the TFAR-TS-Channel-Setter-x mod for the specific profile, i.e 1 for server 1, etc.

![image-2.png](./docs/image-2.png)

6. Repeat steps 3 - 5 for each server profile.
7. Done, start the servers and you're good to go.
